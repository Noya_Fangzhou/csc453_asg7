----------------------
Group Role Assignment
----------------------

Fangzhou Liu -> 
Quality Assurance and Project Leader 
- in charge of group coordination and integration testing between modules 

Yuxiao Chen -> 
Revlog Developer 
- in charge of the Revlog module, including implementation and unit testing


Tianxin Xie -> 
Filelog/Manifest/Changelog Developer 
- in charge of the Filelog/Manifest/Changelog modules, including implementation and unit testing


Xingdi Tan ->
Repository Developer 
-  in charge of the Repository module, including implementation and unit testing 


-------------------------------
Group Bitbucket Repository URL
-------------------------------
https://Noya_Fangzhou@bitbucket.org/Noya_Fangzhou/csc453_final_proj.git