require 'digest/sha1'
require './compress_encode.rb'
$nullid =  Digest::SHA1.hexdigest('nullid')[0,12]
class Revlog
    
    include CompressEncode

    attr_accessor :indexfile
    attr_accessor :datafile
    def initialize(indexfile, datafile)
        @indexfile = indexfile
        @datafile = datafile
        # puts "datafile: #{self.datafile} in init\nindexfile: #{self.indexfile} in init"
        @index = [] #rev
        @nodemap = {-1 => $nullid, $nullid => -1}
        begin
            n = 0

            file = open(@indexfile)
            while line = file.gets
                temp = line.unpack("llllA12ll")
                @nodemap[temp[4]] = n #将nodeid 和rev对应
                @index << temp
                n += 1
            end
            rescue
            # puts "IO error"
        end
        return self
    end
    
    def open(fn, mode="r")
         # puts "I'm here open in Revlog"
        return File.new(fn,mode)
    end
    
    def tip()
        if @index.nil?
            return -1
        else
            return @index.length - 1 #返回tip
        end
    end
    
    def node(rev)
        if(rev < 0 )
            return $nullid
            else
            # puts "@index nil? = #{@index.nil?}"
            return @index[rev][4]
        end
    end
    
    def rev(node)
        # puts "#{node} in #{@nodemap}"
        return @nodemap[node] #返回节点对应的rev
    end
    
    def parents(rev)
        return @index[rev][5,6] #返回节点的父亲（之前的版本）的rev，一个节点最多有两个parent
    end
    
    def start(rev)
        return @index[rev][1].to_i #被修改部分返回offset
    end
    
    def length(rev)
        return @index[rev][2].to_i #返回被修改部分的长度
    end
    
    def fend(rev)
        return start(rev) + length(rev) - 1#被修改文件的结尾
    end
    
    def base(rev)
        return @index[rev][3].to_i #基准版本的rev
    end

    def nodemap()
        return @nodemap
    end
    
    def revisions(list)
        list.each do |rev|
            puts rev
            puts revision(rev)
            
        end
    end
    
    def revision(rev)
        result = []
        if rev == -1
            return ""
        else
            start = start(rev) #基准版本修改部分的偏移量
            fend = fend(rev)
            begin
                n = 1
                file = open(@datafile)
                # puts "datafile = #{@datafile}"
                while line = file.gets
                    # puts "line #{line}"
                    # puts fend
                    # puts "line #{Zlib::Inflate.inflate(line)}"
                    if  (start <= n and n <= fend)
                        # result << Base64.decode64(line)
                        result << decode_decompress(line)
                    elsif n > fend
                        break
                    end
                    n += 1
                end
                #rescue
                #puts "IO error"
            end
        end
        # puts "result!!!!!!!!!!!!! = #{result}"
        return result
    end
    
    def addrevision(text, p1=nil, p2=nil)
        # puts self.class
        # puts "#{self.class} datafile #{self.datafile} in addrevision"
        # puts "#{self.class} indexfile #{self.indexfile} in addrevision"
        if text == nil
            text = ""
        end
        if p1 == nil
            p1 = tip()
        end
        if p2 == nil
            p2 = -1
        end
        t = tip()
        n = t + 1
        old_end = 0
        line_num = 0
        begin
            if n != 0
                file = open(@datafile)
                while (line = file.gets)
                    old_end += 1
                end
                file.close
            end
            #write_datafile
            file = open(self.datafile,'a+')
            temp = text.split("\n")
            
            temp.each do |i|
                tempi = compress_encode(i)
                #puts "i after compress is #{Zlib::Inflate.inflate(i)}\n"
                file.print(tempi)
                file.print("\n")
                # file.print(" || ")
                line_num += 1
            end
            file.close()
        end
        #write_indexfile
        #rev = n
        #start
        #len
        #nodeid
        #p1
        #p2
        nodeid = Digest::SHA1.hexdigest(text)[0,12]
        entities = [n, old_end+1,line_num,0,nodeid,p1,p2]
        @index << (entities)
        @nodemap[nodeid] = n
        file = open(@indexfile, "a")
        num = 0
        #p entities
        xx = entities.pack("llllA12ll")
        file.print(xx + "\n")
        #p xx.unpack("llllA12ll")
        '''
        entities.each do|i|
            file.print(i)
            #puts num
            if num == 6
                file.print("\n")
            else 
                file.print(" ")
            end
            num += 1
        end
        '''
        #file
        file.close()
        return n
    end

    def mergedag(other)
        amap = @nodemap
        bmap = other.nodemap

        old = i = self.tip()
        l = []

        range = (0...other.tip() + 1)

        range.each do |r|
            id = other.node(r)#id = node_id
            if not amap.include?(id)
                i += 1
                (x, y) = other.parents(r)#get 
                xn, yn = other.node(x.to_i), other.node(y.to_i)
                l << [r, amap[xn], amap[yn]]
                amap[id] = i
            end
        end

        l1 = []#used to store all revision
        l.each do |e|
            l1 << e[0]
        end
        r = other.revisions(l1)
        #p r

        now = 0
        l.each do |e|
            t = r[now][0]
            self.addrevision(t, e[1], e[2])
            now += 1
        end

        return old, self.tip()
    end

    def merge(other)
        # this is where to handle rollback
        (o, n) = self.mergedag(other)
        return self.resolvedag(o, n)
    end

    def resolvedag(old, new)
        if old == new
            return nil
            #not merge
        end
        a = self.ancestor(old, new)
        print old, new, a
        if old == a
            return new
        end
        return self.merge3(old, new, a)
    end

    def merge3(my, other, base)
        t = revision(my)
        return self.addrevision(t, my, other)
    end    

    
end