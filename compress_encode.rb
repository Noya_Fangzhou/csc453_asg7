require 'zlib'
require 'base64'

module CompressEncode

	# compress and encode the string
	# first compress the stirng, then use base64 to erase the newly generated \n
	# then substitude the remain \n to agcdefg
	def compress_encode(string)

		comp_str = Zlib::Deflate.deflate(string)

		encode_str = Base64.encode64(comp_str)

		final_str = ""

		encode_str.split("\n").each do |comp|
			final_str += (comp + "abcdefg")
		end

		#puts "string before compress: #{string}\nstring after compress: #{final_str}"

		return final_str
	end


	# decompress and deencode the string
	# first substitude the abcdefg to \n
	# then do the decode and decompress process
	def decode_decompress(string)

		comp_str = ""

		string.split("abcdefg").each do |comp|
			comp_str += (comp + "\n")
		end

		orig_str = Zlib::Inflate.inflate(Base64.decode64(comp_str))

		#puts "string after compress: #{string}\nstring before compress: #{orig_str}"

		return orig_str
		
	end
end