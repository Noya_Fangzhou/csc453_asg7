require './Repository.rb'
require './revlog.rb'
require './filelog_manifest_changelog.rb'


def run
  puts "**********************************************************"
  puts "                Welcome to Mercurial v0.1                 "
  puts
  puts "Current Time: #{Time.new.inspect}"
  puts
  puts 
  puts "Repository Engineer:                 Xingdi Tan"
  puts "Revlog Engineer:                     Yuxiao Chen "
  puts "Filelog/Manifest/Changeset Engineer: Tianxin Xie"
  puts "QA Leader:                           Fangzhou Liu"
  puts
  puts "***********************************************************"





  while true
    puts "mercurial>"
    comm = STDIN.gets
    comm_ary = comm.split(" ")
    if comm_ary[1] == "create"
      if Dir.exist?("./.hg")
        puts "ERROR! the repository has already been created"
        next
      end
      puts "create"
      @repo = Repository.new(".",1)

    elsif comm_ary[1] == "add"
      puts comm_ary.count
      if comm_ary.count <= 2
        puts "ERROR! the correct command for add should be: \"hg add [file_to_add, ...]\""
        next
      end
      puts "add"
      file_list = Array.new
      comm_ary.each_with_index do |e, i|
        if i >=2
          file_list << e
        end
      end
      @repo.add(file_list)

    elsif comm_ary[1] == "delete"
      puts "delete"
      if comm_ary.count <= 2
        puts "ERROR! the correct command for delete should be: \"hg delete [file_to_delete, ...]\""
        next
      end
      file_list = Array.new
      comm_ary.each_with_index do |e, i|
        if i >= 2
          file_list << e
        end
      end
      @repo.delete(file_list)

    elsif comm_ary[1] == "commit"
      puts "commit"
       @repo.commit

    elsif comm_ary[1] == "checkout"
      if (comm_ary.count == 2)
        @repo.checkout(@repo.changelog.tip())
      elsif (comm_ary_count == 3)
        @repo.checkout(comm_ary[2].to_i)
      else
        puts "ERROR! the correct command for checkout should be: \"hg checkout [rev]\" or \"hg checkout\" "
        next
      end

    elsif comm_ary[1] == "stat"
      puts "diffdir"
      @repo.diffdir

    elsif comm_ary[1] == "quit"
      puts "Saving current status........"
      puts "quit"
      break

    elsif comm_ary[1] == "help"
      puts "help"
      print_help

    else
      puts "ERROR! command not find"
      puts "please type hg help to see all commands"
    end
  end
end

def print_help
  puts "--------------------------------COMMAND LIST------------------------------------\n"
  puts
  puts "hg create:           create a new repository at the current path\n"
  puts "hg add file_list:    add all files in the file_list to the repository\n"
  puts "hg delete file_list: delete all files in the file_list from the repository\n"
  puts "hg commit:           commit all changes to the repository\n"
  puts "hg check rev:        checkout the specific rev info of the current directory, if no rev given, it will return the\n"
  puts "                     last version of the directory"
  puts "hg quit:             quit the mercurial\n"
  puts "hg help:             list all the help command\n"
  puts
  puts "------------------------------------END---------------------------------------------\n"
end


run
