require './revlog.rb'
require './filelog_manifest_changelog.rb'
require 'fileutils'

class Repository
    attr_accessor :changelog
    attr_accessor :manifest
    def initialize(path=nil, create=false)
        if not path
            p = Dir.pwd
            while not File.directory?(File.join(p,'.hg'))
                p = File.dirname(p)
                raise RuntimeError, 'No repo found' if p == '/'
            end
            path == p
        end
        @root = path
        path = File.join(@root, ".hg")
        @path = path
        
        if create
            Dir.mkdir(path)
            Dir.mkdir(File.join(path,"data"))
            Dir.mkdir(File.join(path,"index"))
        end
        self.manifest = Manifest.new(self)
        self.changelog = Changelog.new(self)
        
        begin
            @current = self.open("current").readline().to_i
            
            rescue
            @current = nil
        end
        
    end
    
    def open(p,mode="r")
        f = self.join(p)
        if mode == "a+" and File.file?(f)
          s = File.stat(f)
          if s.nlink > 1
            #File(f+".tmp", "w+").write(File(f).read())
            File.open(f+".tmp","w+") do |f1|
              File.open(f,"r") do |f2|
                f2.each_line do |line|
                  f1.puts line
                end
              end
            end
            File.rename(f+".tmp", f)
          end
        end
        return File.new(self.join(p),mode)
    end
    
    def join(f)
        return File.join(@path, f)
    end
    
    def file(f)
        return Filelog.new(self,f)
    end
    
    def merge(other)
        
    end
    
    def commit

        begin
          update = []
          f = self.open("to-add")
          f.each_line do |l|
            update << l[0..-2]
        end
        rescue Errno::ENOENT
          update = []
        end
        begin
          delete = []
          f = self.open("to-delete")
          f.each_line do |l|
            delete << l[0..-2]
          end
        rescue Errno::ENOENT
          delete = []
        end

        #check in files
        create = {}
        update.each do |f|
            r = Filelog.new(self,f)
            # t = File.open(f).read()
            # r.addrevision(t)
            text = ""
            File.open(f) do |f1|
                f1.each do |line|
                  text = text + line
                end
                r.addrevision(text)
            end
            create[f] = r.node(r.tip())
        end

        #update manifest
        old = @manifest.manifest(@manifest.tip())
        old.update(create)
        delete.each do |f|
          old.delete(f)
        end
        rev = @manifest.addmanifest(old)

        #add changeset
        create = create.keys
        create.sort ##
        n = @changelog.addchangeset(@manifest.node(rev), create, "commit")
        @current = n
        #self.open("current", "w+").write(@current.to_s)
        self.open("current", "w+"){|ff| ff.write(@current.to_s) }


        toadd = self.join("to-add")
        FileUtils.rm_rf(toadd) if update
        todelete = self.join("to-delete")
        FileUtils.rm_rf(todelete) if delete

    end

    def merge(other)
        changed = Hash.new
        created = Hash.new

        def accumulate(text)
            files = self.changelog.extract(text)[3]
            for f in files
                puts "#{f} chagned"
                changed[f] = 1
            end
        end
        # begin the import/merge of changesets
        puts "begin the changeset merge\n"
        co, cn = self.changelog.mergedag(other.changelog)

        if co == cn
            return
        end

        # merge all files changed by the changesets,
        # keeping track of the new tips

        # 返回一个Array, 包含所有的文件名
        changed = changed.keys()
        # 按文件名首字母排序
        changed.sort()
        for f in changed
            puts "merging #{f}\n"
            f1 = Filelog.new(self, f)
            f2 = Filelog.new(other, f)
            rev = f1.merge(f2)
            if not (rev == 0)
                created[f] = f1.node(rev)
            end
        end

        # begin the merge of the manifest
        puts "begin the manifest merge\n"
        mm, mo = self.manifest.mergedag(other.manifest)
        ma = self.manifest.ancestor(mm, mo)

        # resolve the manifest to point to all the merged files
        puts "resolving manifests"
        mmap = self.manifest.manifest(mm) # mine
        omap = self.manifest.manifest(mo) # other
        amap = self.manifest.manifest(ma) # ancestor
        nmap = Hash.new

        for f, mid in mmap.items()
            if omap.include?(f)
                if not (mid == omap[f])
                    nmap[f] = created.get(f, mid) # use merged version
                else
                    nmap[f] = created.get(f, mid) # they're the same
                end
                omap[f].delete
            elsif amap.include?(f)
                if not (mid == amap[f])
                    # doing nothing
                else
                    # doing nothing
                end
            else
                nmap[f] = created.get(f, mid)
            end
        end

        mmap = nil

        for f, oid in omap.items()
            if amap.include?(f)
                if not (oid == amap[f])
                    # this is the nasty case, we should prompt here too
                    # do nothing
                else
                    # probably safe
                    # do nothing
                end
            else
               nmap[f] = created.get(f, mid) # remote created it
            end
        end

        nmap = nil
        amap = nil

        nm = self.manifest.addmanifest(nmap, mm, mo)
        node = self.manifest.node(nm)

        # Now all files and manifests are merged, we add the changed files
        # and manifest id to the changelog
        print "committing merge changeset"
        created = created.keys()
        created.sort()
        if co == cn
            cn = -1
        end
        # we should give the user an opportunity to edit the changelog desc
        self.changelog.addchangeset(node, created, "merge", co, cn) # node, manifest, list, desc, p1, p2
    end
    
    def checkdir(path)
        d = File.dirname(path)
        return if not d
        if not File.directory?(d)
            self.checkdir(d)
            Dir.mkdir(d)
        end
    end
    
    def checkout(rev)
        change = @changelog.changeset(rev)
        mnode = change[0]
        mmap = @manifest.manifest(@manifest.rev(mnode))

        st = self.open("dircache","w+")
        l = mmap.keys()
        l.sort()##
        l.each do |f|
          r = Filelog.new(self,f)
          t = r.revision(r.rev(mmap[f]))
          begin
            File.open(f,"w+") do |file|
                t.each do |line|
                    File.write(file,line+"\n",File.size(file))
                end       
            end
          rescue
            self.checkdir(f)
            File.open(f,"w+") do |file|
                t.each do |line|
                    File.write(file,line+"\n",File.size(file))
                end       
            end
          end

          s = File.stat(f)
          e = [s.mode, s.size, s.mtime.to_i, f.length]
          #File.write(st, s.mode.to_s+" | " + s.size.to_s+" | " + s.mtime.to_s+" | " + f.length.to_s+" | " + f +"    ||    ", File.size(st))
          h = e.pack('>llll')
          File.write(st, h + f +"\n", File.size(st))
        end
        @current = change
        ff = self.open("current","w+")
        File.write(ff,@current.to_s,File.size(ff))
    end
    
    def diffdir(path)
        st = self.open("dircache", "r")
        dc = Hash.new
        # p st
        # p '!!!!!'
        st.each_line do |line|

          uline = line.unpack('>llll')
          l = uline[3]
          f = line[16..-2]
          
          dc[f] = uline[0..3]
        end

        changed = []
        added = []
        dir,subdirs,files = os_walk(@root)
        # puts "file = #{files}"

        d = dir[(@root.size)-1 .. -1]
        subdirs -= [".hg"] if subdirs.include?(".hg")

        files.each do |f|
            # puts "d = #{d}"
          fn = File.join(d, f)
          # puts "f = #{f}"
          s = File.stat(fn)
          
          if dc.has_key?(" "+f)
            c = dc[" "+f]
            dc.delete(" "+f)
            # puts "c in has_key is #{c}"
            if c[1] != s.size # c=[33188, 50, 1479515345, 24]
              changed << f
              p "C " + f
            elsif c[0] != s.mode or c[2] != s.mtime
              #p "ffffff"
              t1_arr = []
              t1 = File.new(fn)
              t1.each_line do |line|
                t1_arr << line
              end
              t2 = self.file(f).revision(@current-1) ###########Here is the problem########
              
              
            if t1_arr.size != t2.size
                changed << fn
                p "C "+fn
            else
                index = 0
              t2.each do |t2_e|
                if t2_e != t1_arr[index]
                    changed << fn
                    p "C "+fn
                    break
                end
                index += 1

              end
          end
              '''
              if !FileUtils.compare_file(t1, t2) # t1 != t2
              # if t1 != t2    ####################################Check conflict.
                changed << fn
                p "CC "+fn
              end
              '''
            end
          else
            added << fn
            p "A " + fn
          end
        end


        deleted = dc.keys
        deleted.sort
        # puts "deleted = #{deleted}"
        deleted.each do |f|
          p "D " + f
        end

    end
    
    def os_walk(dir)
        root = Pathname(dir)
        files, dirs = [], []
        Pathname(root).find do |path|
            unless path == root
                dirs << path if path.directory?
                files << path if path.file?
            end
        end
        root_re = []
        files_re = []
        dirs_re = []
        
        files.each do |i|
            files_re << i.to_s
        end
        
        dirs.each do |i|
            dirs_re << i.to_s.split("/")[-1]
        end
        return [dir,dirs_re,files_re]
    end
    
    def add(list)
        if (list.count == 0)
          raise RuntimeError, "List cannot be empty!"
        end
        list.each do |fn|
          if (not File.exist?(fn))
            raise RuntimeError, "File #{fn} does not exist!"
          end
        end
        al = self.open("to-add", "a+")
        st = self.open("dircache", "a+")
        list.each do |f|
          File.write(al,f+"\n",File.size(al))
          s = File.stat(f)
          e = [s.mode, s.size, s.mtime.to_i, f.length]
          h = e.pack('>llll')

          File.write(st, h +" "+ f +"\n", File.size(st))
        end
    end
    
    def delete(list)
        if (list.count == 0)
            raise IOError, "List cannot be empty!"
        end
        list.each do |fn|
            if (not File.exist?(fn))
                raise IOError, "File #{fn} does not exist!"
            end
        end
        dl = self.open("to-delete", "a+")
        list.each do |f|
            File.write(dl, f + "\n", File.size(dl))
        end
    end
    
end
