require 'digest'
require './revlog.rb'

# class Revlog
# 	def initialize(indexfile, datafile)
# 		p "initialize of Revlog: indexfile = ", indexfile
# 		p "initialize of Revlog: datafile = ", datafile
# 		@indexfile = indexfile
# 		@datafile = datafile
# 	end

# 	def revision(rev)
# 		return "" if rev == -1
#         return "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\nUserUserUserUserUserUserUserUserUserUserUserUserUserUserUserUserUserUserUserUser\ndatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedatedate\nother1other1other1other1other1other1other1other1other1other1other1other1other1other1other1other1other1other1\nother2other2other2other2other2other2other2other2other2other2other2other2other2other2other2other2other2other2other2\nother3other3other3other3other3other3other3other3other3other3other3other3other3other3other3other3other3other3other3other3\nother4other4other4other4other4other4other4other4other4other4other4other4other4other4other4other4other4other4other4\nother5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5other5\ndescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdesc"
# 	end

# 	def addrevision(text, p1=nil, p2=nil)
# 		return text, 5
# 	end
# end



class Filelog < Revlog
    #attr_reader :repository
    
    def initialize(repository, path)
        # p "filelog initialize"
        @repository = repository
        @path = path
        super("index/"+path, "data/"+path)
    end
    
    def open(file, mode = "r")
        # puts "I'm here open in filelog"
        return @repository.open(file, mode)
    end


    
end

class Manifest < Revlog
    #attr_reader :repository
    
    def initialize(repository)
        @repository = repository
        #	p "manifest @repo", repository
        super("00manifest.i","00manifest.d")
    end
    
    def open(file, mode="r")
        return @repository.open(file, mode)
    end
    
    def manifest(rev)
        text = revision(rev)
        # puts "text = #{text}"
        if text.empty?
            puts "text empty\nc"
            return {}
        end
        map = Hash.new
        # text_size = text.count
        # text_last = text[text_size-1]
        # puts "text in #{__method__}: #{text}"
        # context = text[0]
        text.each do |e|
            # puts "e = #{e}"
            for l in e.split("\n") do
                map[l[12, l.length-12]] = l[0...12]
            end
        end
        # p "map: ", map
        return map
    end
        
        def addmanifest(map, p1=nil, p2=nil)
            files = map.keys
            files.sort
            text = ""
            for f in files do
                text = text + (map[f] + f + "\n")
            end
            # print "addmanifest: text: \n", text
            return self.addrevision(text, p1, p2)
            end
        end
        
        
        class Changelog < Revlog
            include Enumerable
            def initialize(repository)
                @repository = repository
                # p "Changelog @repository", @repository
                super("00changelog.i","00changelog.d")
            end
            
            def open(file, mode="r")
                return @repository.open(file, mode)
            end
            
            def extract(text)
                l = Array.new
                text.each do |e|
                    l << e
                end
                doc = l[0].split(" ")
                manifest = doc[0]
                # p "extract: manifest: ", manifest
                user = doc[1]
                date = doc[2]
                files = Array.new
                # puts doc[3...-1]
                for f in doc[3...-1] do
                    files << f
                end
                desc = doc[-1]
                # puts "user: #{user}"
                # puts "data: #{date}"
                # puts "file: #{files}"
                # puts "desc: #{desc}"
                l2 = Array.new
                l2 << manifest
                l2 << user
                l2 << date
                l2 << files
                l2 << desc
                return l2
            end
            
            def changeset(rev)
                # p "changeset: call extract: ", extract(revision(rev))
                return extract(revision(rev))
            end
            
            def addchangeset(manifest, list, desc, p1=nil, p2=nil)
                user = ENV["USER"]
                date = Time.now.strftime('%m/%d/%Y-%H:%M:%S').to_s  
                list.sort
                l=[manifest, user, date] + list + ["",desc] #["",desc ] is an Array
                text = ""
                for e in l
                    text += (e.to_s+" ")
                end
                return self.addrevision(text, p1, p2)
            end
        end
        
        # class Repository
        # 	def initialize(path=nil, create=0)
        # 		@path = path
        # 		p "repository path: " + @path
        # 	end
        
        # 	def open(path, mode="r")
        # 		p "filelog.open using repository.open"
        # 		return true
        # 	end
        # end
        
        #  begin
        #      rev = 3
        #      path = "c:/test"
        #      repo = Repository.new(path)
        #      p repo
        #      of = Filelog.new(repo, path)
        #      p ":::::::::: manifest ::::::::::"
        #      manObj = Manifest.new(repo)
        #      map = manObj.manifest(3)
        #      manObj.addmanifest(map)
        #      p ":::::::::::: Changelog :::::::::::"
        #      chanObj = Changelog.new(repo)
        #      reObj = Revlog.new("index 123","data 123")
        #      text = reObj.revision(rev)
        #      res = chanObj.extract(text)
        #      p "the res of extract: ",res
        #      res2 = chanObj.changeset(rev)
        #      manifest = "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv"
        #      desc = "descdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdescdesc"
        #      list = [1,2,3]
        #      resaddc = chanObj.addchangeset(manifest, list, desc)
        #  end
